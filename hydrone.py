"""
Hydrone KBE Application \n
Avy B.V.

Main file
"""

from parapy.core.snapshot import read_snapshot
from parapy.gui import display
from hydrone_app import Drone
from tkinter import Tk, messagebox
from tkinter.filedialog import askopenfilename

# Ask whether user wants to open previous save
Tk().withdraw()
user_answer = messagebox.askyesno('Open previous save',
                                  'Do you want to open a previous save?')

if user_answer:
    save_path = askopenfilename(title="Open previous save",
                                filetypes=[('json', '.json')])
    with open(save_path, "r") as f:
        obj = read_snapshot(f)

else:
    obj = Drone()

display(obj, autodraw=True)
