#!/usr/bin/env python3
"""
Hydrone KBE Application \n
Avy B.V.

Main Aircraft Class
"""

__author__ = "Liam Megill"
__date__ = "Mon 14 Mar 2022"
__copyright__ = "Copyright 2022, Avy B.V."

# Imports
from parapy.core import Input, Part, Attribute, action, derived, child
from parapy.core.snapshot import write_snapshot, read_snapshot
from parapy.geom import GeomBase, translate, rotate, rotate90, MirroredShape, Sphere
from parapy.core.widgets import Dropdown
from parapy.core.validate import Positive, GreaterThan, And, LessThanOrEqualTo
from hydrone_app import Pod, LiftingSurface, Propeller, wing_loading, mass_iteration
import math as m
import pandas as pd
import os
import datetime
import yaml
import json
import sys

_module_dir = os.path.dirname(__file__)
COMP_DIR = os.path.join(_module_dir, 'components', '')

# Constants
g = 9.81
e_H2 = 142.


class Drone(GeomBase):
    """HyDrone KBE Application | Avy B.V. \n
    Class: Drone \n
    Defines a drone...

    Inputs:
    """
    
    #
    # --- Inputs --- #
    #
        
    # Main requirements
    architecture = Input('hydrogen', widget=Dropdown(['hydrogen', 'battery']))
    tail = Input(True, widget=Dropdown([True, False]))
    m_pl = Input(10., validator=Positive(incl_zero=True))
    aspect_ratio = Input(12., validator=Positive())
    range_cruise = Input(100., label="Out-Return Range", validator=Positive())  # TODO: one-way of two-way?
    range_bat_charge = Input(50., validator=Positive())
    time_mc = Input(600., validator=Positive())  # max time in MC mode, power from batteries only
    V_cruise = Input(22., validator=Positive())  # [m/s] cruise speed

    # Layout description
    # num_pods = Input(1, widget=Dropdown([1, 3]))  # TODO: add capability of multiple pods
    payload_under_middle_pod = Input(True)

    # Fuel Tank
    tank_data = pd.read_csv(os.path.join(COMP_DIR, 'hydrogen_tanks.csv'), index_col="Name")
    tank_name = Input("MC2L", widget=Dropdown(tank_data.index.values.tolist()), label="Hydrogen Tank Choice")
    num_tanks = Input(derived)

    # Fuel Cell
    fc_data = pd.read_csv(os.path.join(COMP_DIR, 'fuel_cells.csv'), index_col="Name")
    fc_name = Input("IE800", widget=Dropdown(fc_data.index.values.tolist()), label="Fuel Cell Choice")
    num_fc = Input(derived)
    
    # Battery Data
    bat_data = pd.read_csv(os.path.join(COMP_DIR, 'foxtech_batteries.csv'), index_col="Name")
    bat_name = Input("12S 27000", widget=Dropdown(bat_data.index.values.tolist()), label="Battery Choice")
    num_bat = Input(derived)

    # optimisation code
    CD0 = Input(0.025, validator=Positive())  # TODO: calculate CD0
    CL_max = Input(1.2, validator=Positive())
    disk_loading = Input(10., validator=Positive())
    oswald_factor = Input(0.7, validator=Positive())
    fom_mc = Input(0.75, validator=And(Positive(), LessThanOrEqualTo(1.)))
    num_mc_motors = Input(4, widget=Dropdown([4, 8, 12, 16]))  # re-work validator
    fw_arch = Input('pusher', widget=Dropdown(['pusher', 'tractor']))
    num_fw_motors = Input(2, widget=Dropdown([1, 2, 4, 6, 8]))
    rho = Input(1.225, validator=Positive())
    roc_fw = Input(4., validator=Positive())
    roc_mc = Input(2., validator=Positive())
    SprojS = Input(1.3, validator=Positive())  # TODO: calculate projected area
    tau_bat = Input(0.2, validator=And(Positive(incl_zero=True), LessThanOrEqualTo(1.)))
    TW_mc_max = Input(2., validator=GreaterThan(1.))  # thrust-to-weight ratio in MC mode
    V_stall = Input(derived)  # [m/s] stall speed

    @V_stall.getter
    def V_stall(self):
        """From requirements."""
        return self.V_cruise - 5.

    # Not working yet
    # @num_fw_motors.on_slot_change
    # def num_fw_motors(self, value):
    #     if (value == 1) and (self.fw_arch == 'tractor'):
    #         self.fw_arch = 'pusher'
    #     elif (value != 1) and (self.fw_arch == 'pusher'):
    #         self.fw_arch = 'tractor'

    # @fw_arch.on_slot_change
    # def fw_arch(self, value):
    #     if (value == 'tractor') and (self.num_fw_motors == 1):
    #         self.num_fw_motors == 2
    #     elif (value == 'pusher') and (self.num_fw_motors != 1):
    #         self.num_fw_motors == 1

    efficiencies = Input({"eta_prop_cl": 0.85,
                          "eta_prop_cr": 0.95,
                          "eta_cbl": 0.99,
                          "eta_esc": 0.95,
                          "eta_bat": 0.92,
                          "eta_em": 0.92,
                          "eta_fc": 0.53,
                          "eta_pdb": 0.99})

    densities = Input({"bat_dens": 250.,
                       "bms_dens": 20.,
                       "cbl_dens": 10.,
                       "em_dens": 5.,
                       "esc_dens": 20.,
                       "fc_dens": 2.5,
                       "h2t_dens": 600.,
                       "pdb_dens": 20.})
    
    # wing inputs
    wing_x = Input(800.)  # TODO: replace with x/l_f or something similar
    
    # multicopter inputs
    mc_fus_offset = Input(100.)  # [mm]
    mc_x_offset_fwd = Input(100.)  # [mm]
    mc_x_offset_aft = Input(100.)  # [mm]
    mc_z_offset = Input(50.)  # [mm]
    mc_prop_radius = Input(derived)  # [mm]

    # fw inputs
    fw_fus_offset = Input(200., validator=Positive())
    fw_tip_offset = Input(0., validator=Positive(incl_zero=True))
    fw_z_offset = Input(50., validator=Positive(incl_zero=True))
    fw_prop_radius = Input(200., validator=Positive())

    @mc_prop_radius.getter
    def mc_prop_radius(self):
        return m.sqrt(self.mass_iteration_results[0]["m_to"] / (self.disk_loading * self.num_mc_motors * m.pi)) * 1000.
    
    #
    # --- Attributes --- #
    #

    # Mass, COG & COT location
    @Attribute
    def total_mass(self):
        masses = [self.main_pod.total_mass, self.right_wing.mass]
        if self.tail:
            masses.append(self.vertical_tail.mass)
            masses.append(self.right_horizontal_tail.mass)
        if self.payload_under_middle_pod:
            masses.append(self.payload_pod.total_mass)

        # Elements not in program
        masses.append(self.mass_iteration_results[0]["m_avi"])
        masses.append(self.mass_iteration_results[0]["m_f"])
        masses.append(self.mass_iteration_results[0]["m_ps_mc"])
        masses.append(self.mass_iteration_results[0]["m_ps_fw"])
        masses.append(self.mass_iteration_results[0]["m_pdb"])
        masses.append(self.mass_iteration_results[0]["m_bms"])
        return sum(masses)

    @Attribute
    def total_xcog(self):
        xm_main_pod = self.main_pod.total_xcog * self.main_pod.total_mass
        xm_wing = self.right_wing.lofted_solid.cog.x * self.right_wing.mass
        xm_vtail = self.vertical_tail.lofted_solid.cog.x * self.vertical_tail.mass
        xm_htail = self.right_horizontal_tail.lofted_solid.cog.x * self.right_horizontal_tail.mass
        # TODO: add booms and propulsion system
        xm_lst = [xm_main_pod, xm_wing, xm_vtail, xm_htail]
        if self.payload_under_middle_pod:
            xm_lst.append(self.payload_pod.total_xcog * self.payload_pod.total_mass)
        return sum(xm_lst) / self.total_mass

    @Attribute
    def total_xcot(self):
        x_fwd = sum(self.pos_fwd_props[0])
        x_aft = sum(self.pos_aft_props[0])
        return (x_fwd + x_aft) * 2 / self.num_mc_motors

    @Attribute
    def wing_area(self):
        return self.mass_iteration_results[0]["m_to"] * g / self.mass_iteration_results[2] * 1e6

    @Attribute
    def wing_span(self):
        return m.sqrt(self.wing_area * self.aspect_ratio)

    @Attribute
    def mass_iteration_results(self):
        PW_cl, PW_cr, WS, WP_design = wing_loading(self.aspect_ratio, self.CD0, self.CL_max, self.oswald_factor,
                                                   self.rho, self.V_cruise, self.V_stall, self.roc_fw,
                                                   self.efficiencies, False)
        mass_dict, power_dict = mass_iteration(self.architecture, self.disk_loading, self.densities, self.efficiencies,
                                               self.fom_mc, self.m_pl, PW_cl, PW_cr,
                                               self.range_bat_charge, self.range_cruise, self.rho, self.roc_mc,
                                               self.SprojS, self.tau_bat, self.time_mc, self.TW_mc_max,
                                               self.V_cruise, WP_design, WS)
        return mass_dict, power_dict, WS

    # Fuel Tank
    @Attribute
    def tank_h2_mass(self):
        return self.tank_data.loc[self.tank_name, "Mass_H2"] * 1e-3

    @Attribute
    def tank_mass(self):
        return self.tank_data.loc[self.tank_name, "Mass"]

    @num_tanks.getter
    def num_tanks(self):
        return m.ceil(self.mass_iteration_results[0]["m_f"] / self.tank_h2_mass)

    # Fuel Cell
    @Attribute
    def fc_cont_power(self):
        return float(self.fc_data.loc[self.fc_name, "Cont_power"])

    @Attribute
    def fc_peak_power(self):
        return float(self.fc_data.loc[self.fc_name, "Peak_power"])

    @Attribute
    def fc_mass(self):
        return float(self.fc_data.loc[self.fc_name, "Mass"])

    @num_fc.getter
    def num_fc(self):
        return m.ceil(self.mass_iteration_results[1]["P_fc"] / self.fc_cont_power)

    # Battery
    @Attribute
    def bat_energy(self):
        return float(self.bat_data.loc[self.bat_name, "Energy_Wh"]) * 3600.

    @num_bat.getter
    def num_bat(self):
        return m.ceil(self.mass_iteration_results[1]["E_bat"] / self.bat_energy)

    @Attribute
    def mc_y_range(self):
        min_y = self.main_pod.inner_width / 2 + self.main_pod.skin_thickness + self.mc_fus_offset + self.mc_prop_radius
        max_y = self.wing_span / 2
        return [min_y, max_y]

    @Attribute
    def fw_y_range(self):
        min_y = self.main_pod.inner_width / 2 + self.main_pod.skin_thickness + self.fw_fus_offset + self.fw_prop_radius
        max_y = self.wing_span / 2 - self.fw_tip_offset
        return [min_y, max_y]

    @Attribute
    def pos_fwd_props(self):
        y_pos = []
        if int(self.num_mc_motors / 4.) == 1:
            y_pos.append(self.mc_y_range[0] + (self.mc_y_range[1] - self.mc_y_range[0]) / 2)
        else:
            for i in range(int(self.num_mc_motors / 4.)):
                y_pos.append(
                    self.mc_y_range[0] + i * (self.mc_y_range[1] - self.mc_y_range[0]) / (self.num_mc_motors / 4. - 1))
        x_pos = [
            self.wing_x + y * m.tan(m.radians(self.right_wing.sweep_le)) - self.mc_prop_radius - self.mc_x_offset_fwd
            for y in y_pos]
        z_pos = [self.mc_z_offset + y * m.tan(m.radians(self.right_wing.dihedral)) for y in y_pos]
        return x_pos, y_pos, z_pos

    @Attribute
    def pos_aft_props(self):
        y_pos = []
        if int(self.num_mc_motors / 4.) == 1:
            y_pos.append(self.mc_y_range[0] + (self.mc_y_range[1] - self.mc_y_range[0]) / 2)
        else:
            for i in range(int(self.num_mc_motors / 4.)):
                y_pos.append(
                    self.mc_y_range[0] + i * (self.mc_y_range[1] - self.mc_y_range[0]) / (self.num_mc_motors / 4. - 1))
        x_pos = [self.wing_x + self.right_wing.chord_root + y * m.tan(
            m.radians(self.right_wing.sweep_te)) + self.mc_prop_radius + self.mc_x_offset_aft for y in y_pos]
        z_pos = [self.mc_z_offset + y * m.tan(m.radians(self.right_wing.dihedral)) for y in y_pos]
        return x_pos, y_pos, z_pos

    @Attribute
    def pos_tractor_props(self):
        y_pos = []
        if int(self.num_fw_motors / 2.) == 1:
            y_pos.append(self.fw_y_range[0] + (self.fw_y_range[1] - self.fw_y_range[0]) / 2)
        else:
            for i in range(int(self.num_fw_motors / 2.)):
                y_pos.append(
                    self.fw_y_range[0] + i * (self.fw_y_range[1] - self.fw_y_range[0]) / (self.num_fw_motors / 2. - 1))
        x_pos = [self.wing_x + self.right_wing.chord_root / 4 - (
                    self.right_wing.chord_root - self.right_wing.chord_tip) / self.wing_span / 2 * y for y in y_pos]
        z_pos = [y * m.tan(m.radians(self.right_wing.dihedral)) - self.fw_prop_radius - self.fw_z_offset for y in y_pos]
        return x_pos, y_pos, z_pos

    #
    # --- Actions --- #
    #

    @action(label="Plot Wing Loading Diagrams")
    def plot_wing_loading(self):
        _ = wing_loading(self.aspect_ratio, self.CD0, self.CL_max, self.oswald_factor, self.rho, self.V_cruise,
                         self.V_stall, self.roc_fw, self.efficiencies, True)
        return

    @action(label="Save snapshot to .json file")
    def save_full_drone(self):
        save_name = 'saves\\save_{date:%Y-%m-%d_%H%M%S}.json'.format(date=datetime.datetime.now())
        with open(save_name, 'w') as save_file:
            write_snapshot(self, save_file)
            # yaml.dump(json.load(save_file), default_flow_style=False)

    #
    # --- Parts --- #
    #

    @Part
    def main_pod(self):
        return Pod(pass_down='tank_name, fc_name, bat_name',
                   num_avi=0,
                   num_pl=0,
                   num_fc=self.num_fc,
                   num_bat=self.num_bat,
                   num_tanks=self.num_tanks,
                   pl_mass=self.m_pl,
                   str_mass=self.mass_iteration_results[0]["m_str"] * 0.35,
                   position=self.position)

    @Part
    def payload_pod(self):
        return Pod(suppress=not self.payload_under_middle_pod,
                   pass_down='tank_name, fc_name, bat_name',
                   num_avi=0,
                   num_pl=1,
                   num_fc=0,
                   num_bat=0,
                   num_tanks=0,
                   pl_mass=self.m_pl,
                   str_mass=0.,
                   position=translate(self.position, 'z', -self.main_pod.inner_height / 2 - child.inner_height / 2 - self.main_pod.skin_thickness - child.skin_thickness))

    @Part
    def right_wing(self):
        return LiftingSurface(aspect_ratio=self.aspect_ratio,
                              surface_area=self.wing_area,
                              taper_ratio=1.,
                              sweep_le=0.,
                              mass=self.mass_iteration_results[0]["m_str"] * 0.45,
                              position=translate(self.position, 'x', self.wing_x))

    @Part
    def left_wing(self):
        return MirroredShape(shape_in=self.right_wing.lofted_solid,
                             reference_point=self.position,
                             vector1=self.position.Vz,
                             vector2=self.position.Vx,
                             mesh_deflection=0.0001,
                             color='gray')

    @Part
    def vertical_tail(self):
        return LiftingSurface(suppress=not self.tail,
                              aspect_ratio=1.5,
                              surface_area=120000.,
                              taper_ratio=0.6,
                              sweep_le=30.,
                              mass=self.mass_iteration_results[0]["m_str"] * 0.04,
                              position=translate(rotate90(self.position, 'x'),
                                                 'X', 2500.,
                                                 'Z', self.main_pod.inner_height + self.main_pod.skin_thickness))

    @Part
    def right_horizontal_tail(self):
        return LiftingSurface(suppress=not self.tail,
                              aspect_ratio=4.,
                              surface_area=250000.,
                              taper_ratio=1.,
                              sweep_le=0.,
                              mass=self.mass_iteration_results[0]["m_str"] * 0.06,
                              position=translate(self.position,
                                                 'X', 2500.,
                                                 'Z', self.main_pod.inner_height + self.main_pod.skin_thickness))

    @Part
    def left_horizontal_tail(self):
        return MirroredShape(suppress=not self.tail,
                             shape_in=self.right_horizontal_tail.lofted_solid,
                             reference_point=self.position,
                             vector1=self.position.Vz,
                             vector2=self.position.Vx,
                             mesh_deflection=0.0001,
                             color='gray')

    @Part
    def right_fwd_props(self):
        return Propeller(height=10, radius=self.mc_prop_radius,
                         quantify=int(self.num_mc_motors / 4.),
                         position=translate(self.position,
                                            'x', self.pos_fwd_props[0][child.index],
                                            'y', self.pos_fwd_props[1][child.index],
                                            'z', self.pos_fwd_props[2][child.index]))

    @Part
    def right_aft_props(self):
        return Propeller(height=10, radius=self.mc_prop_radius,
                         quantify=int(self.num_mc_motors / 4.),
                         position=translate(self.position,
                                            'x', self.pos_aft_props[0][child.index],
                                            'y', self.pos_aft_props[1][child.index],
                                            'z', self.pos_aft_props[2][child.index]))

    @Part
    def left_fwd_props(self):
        return Propeller(height=10, radius=self.mc_prop_radius,
                         quantify=int(self.num_mc_motors / 4.),
                         position=translate(self.position,
                                            'x', self.pos_fwd_props[0][child.index],
                                            'y', -self.pos_fwd_props[1][child.index],
                                            'z', self.pos_fwd_props[2][child.index]))

    @Part
    def left_aft_props(self):
        return Propeller(height=10, radius=self.mc_prop_radius,
                         quantify=int(self.num_mc_motors / 4.),
                         position=translate(self.position,
                                            'x', self.pos_aft_props[0][child.index],
                                            'y', -self.pos_aft_props[1][child.index],
                                            'z', self.pos_aft_props[2][child.index]))

    @Part
    def right_booms(self):
        return Pod(quantify=int(self.num_mc_motors / 4),
                   pl_mass=0,
                   num_pl=0,
                   num_avi=0,
                   num_tanks=0,
                   num_fc=0,
                   num_bat=0,
                   str_mass=0.,
                   l_constant=self.pos_aft_props[0][child.index] - self.pos_fwd_props[0][child.index],
                   x_offset=self.pos_fwd_props[0][child.index],
                   l_nosecone=50.,
                   l_tailcone=50.,
                   minimum_inner_width=50.,
                   minimum_inner_height=50.,
                   fillet_radius=25.,
                   position=translate(self.position,
                                      'y', self.pos_fwd_props[1][child.index],
                                      'z', self.pos_fwd_props[2][child.index] - self.mc_z_offset))

    @Part
    def left_booms(self):
        return Pod(quantify=int(self.num_mc_motors / 4),
                   pl_mass=0,
                   num_pl=0,
                   num_avi=0,
                   num_tanks=0,
                   num_fc=0,
                   num_bat=0,
                   str_mass=0.,
                   l_constant=self.pos_aft_props[0][child.index] - self.pos_fwd_props[0][child.index],
                   x_offset=self.pos_fwd_props[0][child.index],
                   l_nosecone=50.,
                   l_tailcone=50.,
                   minimum_inner_width=50.,
                   minimum_inner_height=50.,
                   fillet_radius=25.,
                   position=translate(self.position,
                                      'y', -self.pos_fwd_props[1][child.index],
                                      'z', self.pos_fwd_props[2][child.index] - self.mc_z_offset))

    @Part
    def pusher(self):
        return Propeller(height=10, radius=self.fw_prop_radius,
                         suppress=self.fw_arch == 'tractor',
                         position=translate(rotate90(self.position, 'y'),
                                            'X', self.main_pod.x_components[-1] + self.main_pod.l_tailcone))

    @Part
    def right_tractors(self):
        return Propeller(height=10, radius=self.fw_prop_radius,
                         suppress=self.fw_arch == 'pusher',
                         quantify=self.num_fw_motors,
                         position=translate(rotate90(self.position, 'y'),
                                            'X', self.pos_tractor_props[0][child.index],
                                            'Y', self.pos_tractor_props[1][child.index],
                                            'Z', self.pos_tractor_props[2][child.index]))

    @Part
    def left_tractors(self):
        return Propeller(height=10, radius=self.fw_prop_radius,
                         suppress=self.fw_arch == 'pusher',
                         quantify=self.num_fw_motors,
                         position=translate(rotate90(self.position, 'y'),
                                            'X', self.pos_tractor_props[0][child.index],
                                            'Y', -self.pos_tractor_props[1][child.index],
                                            'Z', self.pos_tractor_props[2][child.index]))

    @Part
    def xcog_pos(self):
        return Sphere(radius=20.,
                      color='red',
                      transparency=0.2,
                      position=translate(self.position, 'x', self.total_xcog, 'y', self.wing_span / 2 + 300.))

    @Part
    def xcot_pos(self):
        return Sphere(radius=20.,
                      color='blue',
                      transparency=0.2,
                      position=translate(self.position, 'x', self.total_xcot, 'y', self.wing_span / 2 + 300.))


if __name__ == '__main__':
    from parapy.gui import display
    obj = Drone()
    display(obj)
