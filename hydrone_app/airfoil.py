#!/usr/bin/env python3
"""
Hydrone KBE Application \n
Avy B.V.

Airfoil Class
"""

from parapy.core import Input, Part, Attribute
from parapy.geom import FittedCurve
from parapy.core.validate import Range, Positive, IsInstance
from hydrone_app import AIRFOIL_DIR
import os


class Airfoil(FittedCurve):
    """

    """

    # inputs
    chord = Input(1., validator=Positive)  # [m]
    airfoil_name = Input("NACA0012", validator=IsInstance(str))
    tc = Input(1., validator=Range(0., 100., incl_max=True))
    mesh_deflection = Input(0.0001)

    @Attribute
    def tc_airfoildata(self):
        """Returns thickness to chord ratio of raw airfoil .dat file if self.tc != 1"""
        if self.tc != 1:
            with open(os.path.join(AIRFOIL_DIR, self.airfoil_name + '.dat'), 'r') as f:
                point_lst1 = []
                for line in f:
                    x, z = line.split(' ', 1)
                    point_lst1.append([float(x), float(z)])

            i = int((len(point_lst1) - 1) / 2)
            lst_tophalf = point_lst1[:i + 1]
            lst_bothalf = point_lst1[i:]
            lst_bothalf2 = lst_bothalf[::-1]

            return max([lst_tophalf[j][1] - lst_bothalf2[j][1]] for j in range(len(lst_tophalf)))[
                       0] * 100
        else:
            return 1

    @Attribute
    def points(self):
        """Returns scaled list of points"""
        with open(os.path.join(AIRFOIL_DIR, self.airfoil_name + '.dat'), 'r') as f:
            point_lst = []
            for line in f:
                x, z = line.split(' ', 1)
                point_lst.append(self.position.translate(
                    "x", float(x) * self.chord,
                    "z", float(z) * self.chord * (self.tc / self.tc_airfoildata)
                ))
        return point_lst


if __name__ == '__main__':
    from parapy.gui import display

    obj = Airfoil()
    display(obj)
