#!/usr/bin/env python3
"""
Hydrone KBE Application \n
Avy B.V.

Fuselage/Pod Class
"""

__author__ = "Liam Megill"
__date__ = "Mon 21 Mar 2022"
__copyright__ = "Copyright 2022, Avy B.V."

from parapy.core import Input, Part, Attribute, derived, child
from parapy.geom import GeomBase, translate, rotate, rotate90, Rectangle, FilletedWire, ThickShell, Ellipse, LoftedShell
from parapy.core.widgets import Dropdown
from parapy.core.validate import Positive, IsInstance, GreaterThanOrEqualTo
from hydrone_app import FuelCell, HydrogenTank, Battery, Payload, COMP_DIR
import math as m
import pandas as pd
import os


class Pod(GeomBase):
    """
    HyDrone KBE Application | Avy B.V. \n
    Class: Pod \n
    Defines a filleted rectangular pod with internal components where necessary. Future designs will have the option
    to have a circular and purely rectangular fuselage.

    Inputs:
    - str_mass \n
    - num_pl \n
    - num_tanks \n
    - num_fc \n
    - num_bat \n
    - num_avi
    """

    #
    # --- Inputs --- #
    #

    shape_type = Input('filleted_rect', widget=Dropdown(['filleted_rect']))  # TODO: options circle and rect
    component_order = Input(["AVI", "BAT", "PL", "FC", "FT"])
    str_mass = Input(validator=Positive(incl_zero=True))
    l_constant = Input(derived)  # length of constant cross-section [mm]
    l_nosecone = Input(300., validator=Positive())  # length of nosecone [mm]
    l_tailcone = Input(100., validator=Positive())  # length of tailcone [mm]
    x_offset = Input(0., doc="Additional x offset to help in positioning")

    # Payload
    num_pl = Input(validator=IsInstance(int))  # number decided in aircraft.py
    pl_mass = Input(10., validator=Positive(incl_zero=True))
    pl_ratios = Input([2, 1, 5])
    pl_density = Input(300., validator=Positive())

    # Fuel Tank
    tank_data = pd.read_csv(os.path.join(COMP_DIR, 'hydrogen_tanks.csv'), index_col="Name")
    tank_name = Input("MC2L", widget=Dropdown(tank_data.index.values.tolist()), label="Hydrogen Tank Choice")
    num_tanks = Input(validator=IsInstance(int))
    tank_arrangement = Input([1, 1])  # tanks arranged in [horizontal, vertical]

    # Fuel Cell
    fc_data = pd.read_csv(os.path.join(COMP_DIR, 'fuel_cells.csv'), index_col="Name")
    fc_name = Input("IE800", widget=Dropdown(fc_data.index.values.tolist()), label="Fuel Cell Choice")
    num_fc = Input(validator=IsInstance(int))
    fc_arrangement = Input([1, 1])

    # Battery
    bat_data = pd.read_csv(os.path.join(COMP_DIR, 'foxtech_batteries.csv'), index_col="Name")
    bat_name = Input("12S 27000", widget=Dropdown(bat_data.index.values.tolist()), label="Battery Choice")
    num_bat = Input(validator=IsInstance(int))
    bat_arrangement = Input([1, 1])  # [horizontal, vertical]

    # Avionics
    num_avi = Input(validator=IsInstance(int))  # TODO: include avionics

    # Size inputs
    inner_width = Input(derived, validator=Positive())  # for rectangle [mm]
    minimum_inner_width = Input(100., validator=Positive())  # [mm]
    inner_height = Input(derived, validator=Positive())  # for rectangle [mm]
    minimum_inner_height = Input(100., validator=Positive())
    fillet_radius = Input(50., )  # for rectangle [mm]

    @fillet_radius.validator
    def fillet_radius(self, value):
        if value > min([self.minimum_inner_height, self.minimum_inner_width]) / 2:
            return False, f'The radius is too large. Try reducing it to less than half of the minimum height or width.'
        else:
            return True

    skin_thickness = Input(5., validator=Positive())  # [mm]
    wall_offset = Input(10., validator=Positive(incl_zero=True))  # [mm]
    component_spacing = Input(10., validator=Positive(incl_zero=True))  # [mm]

    # Derived value calculations
    @inner_width.getter
    def inner_width(self):
        """Calculate the minimum required inner width based on components inside."""
        widths = [self.minimum_inner_width]
        if self.num_fc > 0:
            widths.append(self.fc_arrangement[0] * self.fc_width + 2 * max(self.fillet_radius, self.wall_offset))
        if self.num_tanks > 0:
            widths.append(self.tank_arrangement[0] * self.tank_radius * 2 + 2 * self.wall_offset + (
                        self.tank_arrangement[0] - 1) * self.component_spacing)
        if self.num_bat > 0:
            widths.append(self.bat_arrangement[0] * self.bat_width + 2 * max(self.fillet_radius, self.wall_offset))
        if self.num_pl > 0:
            widths.append(self.pl_width + 2 * max(self.fillet_radius, self.wall_offset))
        return max(widths)

    @inner_height.getter
    def inner_height(self):
        """Calculate the minimum required inner height based on components inside."""
        heights = [self.minimum_inner_height]
        if self.num_fc > 0:
            heights.append(self.fc_arrangement[1] * self.fc_height + 2 * self.wall_offset)
        if self.num_tanks > 0:
            heights.append(self.tank_arrangement[1] * self.tank_radius * 2 + 2 * self.wall_offset)
        if self.num_bat > 0:
            heights.append(self.bat_arrangement[1] * self.bat_height + 2 * self.wall_offset)
        if self.num_pl > 0:
            heights.append(self.pl_height + 2 * self.wall_offset)
        return max(heights)

    @l_constant.getter
    def l_constant(self):
        return self.x_components[-1] - self.l_nosecone - self.x_offset

    #
    # --- Attributes --- #
    #

    # Payload values
    @Attribute
    def pl_volume(self):
        return self.pl_mass / self.pl_density * 1e9  # [mm3]

    @Attribute
    def pl_width(self):
        return (self.pl_volume * self.pl_ratios[0] ** 2 / self.pl_ratios[1] / self.pl_ratios[2]) ** (1 / 3)

    @Attribute
    def pl_height(self):
        return (self.pl_volume * self.pl_ratios[1] ** 2 / self.pl_ratios[0] / self.pl_ratios[2]) ** (1 / 3)

    @Attribute
    def pl_length(self):
        return (self.pl_volume * self.pl_ratios[2] ** 2 / self.pl_ratios[0] / self.pl_ratios[1]) ** (1 / 3)

    # Tank values
    @Attribute
    def tank_h2_mass(self):
        return self.tank_data.loc[self.tank_name, "Mass_H2"] * 1e-3

    @Attribute
    def tank_mass(self):
        return self.tank_data.loc[self.tank_name, "Mass"]

    @Attribute
    def tank_radius(self):
        return self.tank_data.loc[self.tank_name, "Diameter"] / 2

    @Attribute
    def tank_length(self):
        return self.tank_data.loc[self.tank_name, "Length"]

    # Fuel cell values
    @Attribute
    def fc_cont_power(self):
        return float(self.fc_data.loc[self.fc_name, "Cont_power"])

    @Attribute
    def fc_peak_power(self):
        return float(self.fc_data.loc[self.fc_name, "Peak_power"])

    @Attribute
    def fc_mass(self):
        return float(self.fc_data.loc[self.fc_name, "Mass"])

    @Attribute
    def fc_width(self):
        return float(self.fc_data.loc[self.fc_name, "Width"])

    @Attribute
    def fc_depth(self):
        return float(self.fc_data.loc[self.fc_name, "Depth"])

    @Attribute
    def fc_height(self):
        return float(self.fc_data.loc[self.fc_name, "Height"])

    # Battery values
    @Attribute
    def bat_energy(self):
        return float(self.bat_data.loc[self.bat_name, "Energy_Wh"]) * 3600.

    @Attribute
    def bat_width(self):
        return float(self.bat_data.loc[self.bat_name, "Width"])

    @Attribute
    def bat_height(self):
        return float(self.bat_data.loc[self.bat_name, "Height"])

    @Attribute
    def bat_length(self):
        return float(self.bat_data.loc[self.bat_name, "Length"])

    @Attribute
    def bat_mass(self):
        return float(self.bat_data.loc[self.bat_name, "Mass"])

    # Positioning
    @Attribute
    def x_components(self):
        """x-axis positioning of all components inside pod"""
        starts = [self.l_nosecone + self.x_offset]
        for comp in self.component_order:
            if comp == "FC" and self.num_fc > 0:
                add_length = m.ceil(self.num_fc / (self.fc_arrangement[0] * self.fc_arrangement[1])) * (self.fc_depth + self.component_spacing)
            elif comp == "FT" and self.num_tanks > 0:
                add_length = m.ceil(self.num_tanks / (self.tank_arrangement[0] * self.tank_arrangement[1])) * (self.tank_length + self.component_spacing)
            elif comp == "BAT" and self.num_bat > 0:
                add_length = m.ceil(self.num_bat / (self.bat_arrangement[0] * self.bat_arrangement[1])) * (self.bat_length + self.component_spacing)
            elif comp == "PL" and self.num_pl > 0:
                add_length = self.pl_length + self.component_spacing
            else:
                add_length = 0
            starts.append(starts[-1] + add_length)
        return starts

    @Attribute
    def x_fc_start(self):
        return self.x_components[self.component_order.index("FC")]

    @Attribute
    def x_ft_start(self):
        return self.x_components[self.component_order.index("FT")]

    @Attribute
    def x_bat_start(self):
        return self.x_components[self.component_order.index("BAT")]

    @Attribute
    def x_pl_start(self):
        return self.x_components[self.component_order.index("PL")]

    # Mass and CG locations
    @Attribute
    def total_mass(self):
        m_fc_t = self.fc_mass * self.num_fc
        m_tf_t = self.tank_mass * self.num_tanks
        m_pl_t = self.pl_mass * self.num_pl
        m_bat_t = self.bat_mass * self.num_bat
        # m_avi_t = self.avi_mass * self.num_avi
        return m_fc_t + m_tf_t + m_pl_t + m_bat_t + self.str_mass  # + m_avi_t

    @Attribute
    def total_xcog(self):
        xm_fc = sum([self.fuel_cell[i].fuel_cell.cog.x * self.fuel_cell[i].mass for i in range(self.num_fc)])
        xm_ft = sum([self.fuel_tank[i].cylindrical_section.cog.x * self.fuel_tank[i].mass for i in range(self.num_tanks)])
        xm_pl = sum([self.payload[i].payload.cog.x * self.payload[i].mass for i in range(self.num_pl)])
        xm_bat = sum([self.battery[i].battery.cog.x * self.battery[i].mass for i in range(self.num_bat)])
        xm_str = 0.4 * self.l_total * self.str_mass
        return (xm_fc + xm_ft + xm_pl + xm_bat + xm_str) / self.total_mass

    @Attribute
    def l_total(self):
        return self.l_constant + self.l_nosecone + self.l_tailcone

    #
    # --- Parts --- #
    #

    @Part
    def wire_rect(self):
        return Rectangle(width=self.inner_height,
                         length=self.inner_width,
                         position=translate(rotate(self.position, 'y', 90, deg=True),
                                            'z', self.l_nosecone + self.x_offset))

    @Part
    def wire_rect_fillet(self):
        return FilletedWire(built_from=self.wire_rect,
                            radius=self.fillet_radius)

    @Part
    def wire_rect2(self):
        return Rectangle(width=self.inner_height,
                         length=self.inner_width,
                         position=translate(rotate(self.position, 'y', 90, deg=True),
                                            'z', self.l_constant + self.l_nosecone + self.x_offset))

    @Part
    def wire_rect_fillet2(self):
        return FilletedWire(built_from=self.wire_rect2,
                            radius=self.fillet_radius)

    @Part(in_tree=False)
    def lofted_shell(self):
        return LoftedShell(profiles=[self.wire_rect_fillet, self.wire_rect_fillet2])

    @Part
    def thick_shell(self):
        return ThickShell(built_from=self.lofted_shell,
                          offset=self.skin_thickness,
                          transparency=0.5,
                          color='gray')

    @Part
    def test_front(self):
        return Ellipse(major_radius=40, minor_radius=20,
                       position=translate(rotate90(rotate90(self.position, 'z'), 'x'),
                                          'X', self.x_offset),
                       color='gray')

    @Part
    def test_back(self):
        return Ellipse(major_radius=40, minor_radius=20,
                       position=translate(rotate90(rotate90(self.position, 'z'), 'x'),
                                          'X', self.l_total + self.x_offset))

    @Part
    def test_nose(self):
        return LoftedShell(profiles=[self.wire_rect_fillet, self.test_front],
                           transparency=0.5,
                           color='gray')

    @Part
    def test_tail(self):
        return LoftedShell(profiles=[self.wire_rect_fillet2, self.test_back],
                           transparency=0.5,
                           color='gray')

    #
    # --- Components --- #
    #

    @Part
    def fuel_cell(self):
        return FuelCell(suppress=self.num_fc == 0,
                        fc_name=self.fc_name,
                        quantify=self.num_fc,
                        position=translate(self.position,
                                           'x', self.x_fc_start + m.floor(child.index / (self.fc_arrangement[0] * self.fc_arrangement[1])) * (self.fc_depth + self.component_spacing),
                                           'y', -self.fc_arrangement[0] * self.fc_width / 2 + (child.index % self.fc_arrangement[0]) * (self.fc_width + self.component_spacing),
                                           'z', -self.inner_height / 2 + self.wall_offset + m.floor((child.index - m.floor(child.index / (self.fc_arrangement[0] * self.fc_arrangement[1])) * self.fc_arrangement[0] * self.fc_arrangement[1]) / self.fc_arrangement[0]) * (self.fc_height + self.component_spacing))
                        )

    @Part
    def fuel_tank(self):
        return HydrogenTank(suppress=self.num_tanks == 0,
                            tank_name=self.tank_name,
                            quantify=self.num_tanks,
                            position=translate(self.position,
                                               'x', self.x_ft_start + m.floor(child.index / (self.tank_arrangement[0] * self.tank_arrangement[1])) * (self.tank_length + self.component_spacing),
                                               'y', -self.tank_arrangement[0] * self.tank_radius + (child.index % self.tank_arrangement[0]) * (self.tank_radius * 2 + self.component_spacing),
                                               'z', -self.inner_height / 2 + self.wall_offset + m.floor((child.index - m.floor(child.index / (self.tank_arrangement[0] * self.tank_arrangement[1])) * self.tank_arrangement[0] * self.tank_arrangement[1]) / self.tank_arrangement[0]) * (self.tank_radius * 2 + self.component_spacing))
                            )


    @Part
    def battery(self):
        return Battery(suppress=self.num_bat == 0,
                       bat_name=self.bat_name,
                       quantify=self.num_bat,
                       color='darkblue',
                       position=translate(self.position,
                                          'y', -self.bat_arrangement[0] * self.bat_width / 2 + (child.index % self.bat_arrangement[0]) * self.bat_width,
                                          'z', -self.inner_height / 2 + self.wall_offset + m.floor((child.index - m.floor(child.index / (self.bat_arrangement[0] * self.bat_arrangement[1])) * self.bat_arrangement[0] * self.bat_arrangement[1]) / self.bat_arrangement[0]) * self.bat_height,
                                          'x', self.x_bat_start + m.floor(child.index / (self.bat_arrangement[0] * self.bat_arrangement[1])) * (self.bat_length + self.component_spacing))
                       )


    @Part
    def payload(self):
        return Payload(suppress=self.num_pl == 0,
                       quantify=self.num_pl,
                       ratios=self.pl_ratios,
                       density=self.pl_density,
                       mass=self.pl_mass,
                       position=translate(self.position,
                                          'x', self.x_pl_start,
                                          'y', -self.pl_width / 2,
                                          'z', -self.pl_height / 2)
                       )


if __name__ == '__main__':
    from parapy.gui import display
    obj = Pod(num_avi=0,
              num_pl=0,
              num_fc=0,
              num_bat=0,
              num_tanks=0,
              str_mass=0.)
    display(obj)
