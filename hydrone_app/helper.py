#!/usr/bin/env python3
"""
Hydrone KBE Application \n
Avy B.V.

Helper functions.
"""

__author__ = "Liam Megill"
__date__ = "Wed 16 Mar 2022"
__copyright__ = "Copyright 2022, Avy B.V."


import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

colors = ['#2271B2', '#3DB7E9', '#F748A5', '#359B73', '#D55E00', '#E69F00',
          '#F0E442']
sns.set_context("paper", rc={"lines.linewidth": 2})
sns.set_style("ticks")

# Constants
g = 9.81
e_H2 = 142.


def wing_loading(A, CD0, CL_max, e, rho, V_cr, V_st, RoC_fw, efficiencies, plot_diagrams, WS_lst=None):
    """
    Perform wing loading calculations and plot if desired.
    :param float A: aspect ratio
    :param float CD0: zero-lift drag coefficient
    :param float CL_max: maximum lift coefficient
    :param float e: oswald efficiency factor
    :param float rho: air density [kg/m3]
    :param float V_cr: cruise velocity [m/s]
    :param float V_st: stall velocity [m/s]
    :param float RoC_fw: rate of climb in FW flight mode
    :param dict efficiencies: dictionary of efficiency values
    :param bool plot_diagrams: True plots diagrams in SciView
    :param np.ndarray WS_lst: list of wingloading parameters, default (None) is between 20 and 200 N/m2
    :return:
    """

    eta_prop_cl = efficiencies["eta_prop_cl"]
    eta_prop_cr = efficiencies["eta_prop_cr"]

    if WS_lst is None:
        WS_lst = np.linspace(20, 250, num=40)

    k = 1 / (np.pi * A * e)

    # Climb
    V_cl = np.sqrt(2 / rho * WS_lst * np.sqrt(k / (3 * CD0)))
    q_cl = 0.5 * rho * V_cl ** 2
    TW_cl_lst = RoC_fw / V_cl + q_cl / WS_lst * CD0 + k / q_cl * WS_lst  # TODO
    PW_cl_lst = TW_cl_lst * V_cl / eta_prop_cl

    # Cruise (80% throttle)
    q_cr = 0.5 * rho * V_cr ** 2
    TW_cr_lst = 1 / 0.8 * (q_cr * CD0 / WS_lst + k / q_cr * WS_lst)
    PW_cr_lst = TW_cr_lst * V_cr / eta_prop_cr

    # Stall
    WS_st = 0.5 * rho * V_st ** 2 * CL_max

    # Find design point
    WS_idx = min(range(len(WS_lst)), key=lambda i: abs(WS_lst[i] - WS_st))
    WS = WS_lst[WS_idx]
    PW_cl = PW_cl_lst[WS_idx]
    TW_cl = TW_cl_lst[WS_idx]
    PW_cr = PW_cr_lst[WS_idx]
    TW_cr = TW_cr_lst[WS_idx]
    WP_design = min(1 / PW_cr, 1 / PW_cl)
    TW_design = max(TW_cr, TW_cl)

    if plot_diagrams:
        # Plot wing loading
        fig, ax = plt.subplots()
        ax.plot(WS_lst, 1 / PW_cl_lst, label='Climb', color=colors[0])
        ax.plot(WS_lst, 1 / PW_cr_lst, label='Cruise', color=colors[1])
        ax.axvline(WS_st, label='Stall', color=colors[3])
        ax.plot(WS_lst[WS_idx], WP_design, label='Design', color=colors[2],
                marker='o', linewidth=0)
        ax.set_xlabel('Wing loading [N/m2]')
        ax.set_ylabel('Power loading [N/W]')
        ax.legend(loc='best')
        fig.tight_layout()
        fig.show()

        fig2, ax2 = plt.subplots()
        ax2.plot(WS_lst, TW_cl_lst, label='Climb', color=colors[0])
        ax2.plot(WS_lst, TW_cr_lst, label='Cruise', color=colors[1])
        ax2.axvline(WS_st, label='Stall', color=colors[3])
        ax2.plot(WS_lst[WS_idx], TW_design, label='Design', color=colors[2],
                 marker='o', linewidth=0)
        ax2.set_xlabel('Wing loading [N/m2]')
        ax2.set_ylabel('Thrust loading [N/N]')
        ax2.legend(loc='best')
        fig2.tight_layout()
        fig2.show()

    return PW_cl, PW_cr, WS, WP_design


def mass_iteration(architecture, DL, densities, efficiencies,
                   FoM_mc, m_pl, PW_cl, PW_cr, R_bat_charge, R_cr, rho, RoC_mc, SprojS,
                   tau_bat, t_mc, TWmc_max, V_cr, WP_design, WS):

    # Perform mass iterations to achieve a consistent design.

    eta_bat = efficiencies["eta_bat"]
    eta_em = efficiencies["eta_em"]
    eta_esc = efficiencies["eta_esc"]
    eta_pdb = efficiencies["eta_pdb"]
    eta_fc = efficiencies["eta_fc"]

    bat_dens = densities["bat_dens"]
    bms_dens = densities["bms_dens"]
    cbl_dens = densities["cbl_dens"]
    em_dens = densities["em_dens"]
    esc_dens = densities["esc_dens"]
    fc_dens = densities["fc_dens"]
    h2t_dens = densities["h2t_dens"]
    pdb_dens = densities["pdb_dens"]

    # Initial guess
    m_to = 5 * m_pl
    m_to_i = 4 * m_pl

    while abs(1 - m_to_i / m_to) > 0.001:
        m_to_i = m_to

        # VTOL propulsion
        # climb
        TWmc_cl = 1.2 * (1 + 1 / WS * rho * RoC_mc ** 2 * SprojS)
        Tmc_max = TWmc_max * m_to * g  # or TWmc_cl
        S_disc = Tmc_max / g / DL  # TODO: make sure this is correct
        Pmc_max = Tmc_max ** 1.5 / np.sqrt(2 * rho * S_disc) / FoM_mc

        # hover
        Tmc_hov = m_to * g * 1.1  # assumed 1.1 for now
        Pmc_hov = Tmc_hov ** 1.5 / np.sqrt(2 * rho * S_disc) / FoM_mc
        E_hov = Pmc_hov * t_mc

        m_em_mc = Pmc_max / em_dens / 1000  # [kg] mass of MC electric motor
        m_esc_mc = Pmc_max / eta_em / esc_dens / 1000  # [kg] mass of MC ESC
        m_ps_mc = m_em_mc + m_esc_mc

        # FW propulsion
        # climb
        P_cl = PW_cl * m_to * g

        # cruise
        R_h2 = R_cr * 2  # [km] range between H2 fuelling
        P_cr = PW_cr * m_to * g
        t_cr = R_cr * 1000 / V_cr  # [s] cruising time
        E_cr = P_cr * t_cr  # [J] cruise energy

        # masses
        m_em_fw = 1 / WP_design * m_to * g / em_dens / 1000  # TODO: why is the propeller efficiency not accounted for here?
        m_esc_fw = 1 / WP_design * m_to * g / eta_em / esc_dens / 1000
        m_ps_fw = m_em_fw + m_esc_fw

        m_pdb = Pmc_max / eta_em / eta_esc / pdb_dens / 1000
        m_bms = Pmc_max / eta_em / eta_esc / eta_pdb / bms_dens / 1000

        if architecture == 'hydrogen':
            # battery
            E_bat = (E_hov / eta_em / eta_esc / eta_bat) / (1 - tau_bat)
            m_bat = E_bat / bat_dens / 3600

            # fuel cell
            t_bat_charge = R_bat_charge * 1000 / V_cr
            P_fc = (P_cr + E_hov / 2 /
                         t_bat_charge) / eta_em / eta_esc / eta_pdb
            m_fc = P_fc / fc_dens / 1000
            E_cr_fc = P_fc * R_h2 * 1000 / V_cr

            m_f = E_cr_fc / eta_fc / e_H2 / 1e6
            m_fs = m_f * e_H2 * 1e6 / h2t_dens / 3600

        elif architecture == 'battery':
            E_bat = ((E_hov + E_cr * 2) / eta_em / eta_esc / eta_bat) / (1 - tau_bat)
            m_bat = E_bat / bat_dens / 3600

            m_fc = 0
            m_fs = 0
            m_f = 0
            P_fc = 0

        # total
        m_ps = m_ps_mc + m_ps_fw + m_pdb + m_bms + m_fc + m_bat

        # structure
        m_str = 0.35 * m_to  # 0.3 - 0.4 (Tyan 2017, Gundlach 2014)

        # avionics
        m_avi = 0.05 * m_to  # (Tyan 2017, Gundlach 2014)

        # total mass
        m_oe = m_str + m_avi + m_fs + m_ps
        m_to = m_oe + m_pl + m_f

    m_to = m_to

    mass_dict = {"m_to": m_to, "m_oe": m_oe, "m_f": m_f, "m_ps": m_ps, "m_str": m_str, "m_avi": m_avi, "m_fs": m_fs,
                 "m_ps_mc": m_ps_mc, "m_ps_fw": m_ps_fw, "m_pdb": m_pdb, "m_fc": m_fc, "m_bat": m_bat, "m_bms": m_bms}

    power_dict = {"P_fc": P_fc, "E_bat": E_bat}

    return mass_dict, power_dict

