#!/usr/bin/env python3
"""
Hydrone KBE Application \n
Avy B.V.

Lifting Surface Class
"""

__author__ = "Liam Megill"
__date__ = "Mon 21 Mar 2022"
__copyright__ = "Copyright 2022, Avy B.V."

from parapy.core import Input, Part, Attribute
from parapy.geom import GeomBase, translate, rotate, LoftedSolid
from parapy.core.validate import Positive, IsInstance, Range, And, LessThanOrEqualTo, GreaterThanOrEqualTo
import math as m
from hydrone_app import Airfoil


def change_of_sweep(sweep_m, m_perc, n_perc, taper_ratio, aspect_ratio):
    """Calculates the sweep angle of a line n based on another sweep angle of line m.

    :param float sweep_m: Sweep angle of line m [deg]
    :param float m_perc: Percentage of line m [%] (e.g. 0 for LE, 100 for TE)
    :param float n_perc: Percentage of line n [%]
    :param float taper_ratio: Taper ratio of surface
    :param float aspect_ratio: Aspect ratio of surface
    :return: Sweep angle of line n [deg]
    """
    return m.degrees(m.atan(m.tan(m.radians(sweep_m)) - 4 /
                            aspect_ratio * (n_perc - m_perc) /
                            100. * (1 - taper_ratio) / (1 + taper_ratio)))


class LiftingSurface(GeomBase):
    """
    HyDrone KBE Application | Avy B.V. \n
    Class: Lifting Surface \n
    Defines a simple lifting surface, useful for main wings and for tails. Note that only a half surface is defined
    (the right side). The surface should be mirrored to obtain a full wing.

    Inputs:
    - aspect_ratio \n
    - surface_area \n
    - taper_ratio \n
    - sweep_le \n
    - mass
    """

    # inputs
    airfoil_root_name = Input("NACA0012", validator=IsInstance(str))
    airfoil_tip_name = Input("NACA0012", validator=IsInstance(str))
    aspect_ratio = Input(validator=Positive())
    surface_area = Input(validator=Positive())  # [mm2] full surface area
    taper_ratio = Input(validator=And(Positive(), LessThanOrEqualTo(1.)))
    sweep_le = Input(validator=And(GreaterThanOrEqualTo(0.), LessThanOrEqualTo(45.)))  # [deg]
    mass = Input(validator=Positive(incl_zero=True))  # [kg] full mass

    dihedral = Input(0., validator=And(GreaterThanOrEqualTo(-10.), LessThanOrEqualTo(10.)))  # [deg]
    incidence = Input(0., validator=And(GreaterThanOrEqualTo(-10.), LessThanOrEqualTo(10.)))  # [deg]
    twist_tip = Input(0., validator=And(GreaterThanOrEqualTo(-10.), LessThanOrEqualTo(10.)))  # [deg]
    tc_root = Input(1., validator=Range(0., 100., incl_max=True))
    tc_tip = Input(1., validator=Range(0., 100., incl_max=True))

    # Attributes
    @Attribute
    def span(self):
        return m.sqrt(self.aspect_ratio * self.surface_area)

    @Attribute
    def chord_root(self):
        return 2 * self.surface_area / ((1 + self.taper_ratio) * self.span)

    @Attribute
    def chord_tip(self):
        return self.chord_root * self.taper_ratio

    @Attribute
    def c_mac(self):
        return 2 / 3 * self.chord_root * (1 + self.taper_ratio + self.taper_ratio ** 2) / (1 + self.taper_ratio)

    @Attribute
    def y_mac(self):
        return self.span / 6 * ((1 + 2 * self.taper_ratio) / (1 + self.taper_ratio))

    @Attribute
    def x_lemac(self):
        return self.y_mac * m.tan(m.radians(self.sweep_le))

    @Attribute
    def z_lemac(self):
        return self.y_mac * m.tan(m.radians(self.dihedral))

    # Extra sweep angles
    @Attribute
    def sweep_c4(self):
        """Quarter chord sweep angle [deg]"""
        return change_of_sweep(self.sweep_le, 0., 25., self.taper_ratio, self.aspect_ratio)

    @Attribute
    def sweep_c2(self):
        """Half chord sweep angle [deg]"""
        return change_of_sweep(self.sweep_le, 0., 50., self.taper_ratio, self.aspect_ratio)

    @Attribute
    def sweep_c34(self):
        """Three-quarter chord sweep angle [deg]"""
        return change_of_sweep(self.sweep_le, 0., 75., self.taper_ratio, self.aspect_ratio)

    @Attribute
    def sweep_te(self):
        return change_of_sweep(self.sweep_le, 0., 100., self.taper_ratio, self.aspect_ratio)

    # Parts
    @Part
    def airfoil_root(self):
        return Airfoil(airfoil_name=self.airfoil_root_name,
                       chord=self.chord_root,
                       tc=self.tc_root,
                       position=rotate(self.position, 'y', m.radians(self.incidence)),
                       color='black')
    
    @Part
    def airfoil_tip(self):
        return Airfoil(airfoil_name=self.airfoil_tip_name,
                       chord=self.chord_tip,
                       tc=self.tc_tip,
                       position=translate(rotate(self.position, 'y', m.radians(self.twist_tip + self.incidence)),
                                          'y', self.span / 2,
                                          'x', self.span / 2 * m.tan(m.radians(self.sweep_le)),
                                          'z', self.span / 2 * m.tan(m.radians(self.dihedral))),
                       color='black')
    
    @Part
    def lofted_solid(self):
        return LoftedSolid(profiles=[self.airfoil_root, self.airfoil_tip],
                           ruled=True,
                           mesh_deflection=0.0001,
                           color='gray')


if __name__ == '__main__':
    from parapy.gui import display
    obj = LiftingSurface(aspect_ratio=8.,
                         surface_area=100000.,
                         sweep_le=0.,
                         taper_ratio=1.,
                         mass=0.)
    display(obj)
