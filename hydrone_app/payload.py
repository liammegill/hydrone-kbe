#!/usr/bin/env python3
"""
Hydrone KBE Application \n
Avy B.V.

Payload class
"""

__author__ = "Liam Megill"
__date__ = "Mon 21 Mar 2022"
__copyright__ = "Copyright 2022, Avy B.V."

from parapy.core import Input, Part, Attribute
from parapy.geom import GeomBase, Box
from parapy.core.validate import Positive


class Payload(GeomBase):
    """
    HyDrone KBE Application | Avy B.V. \n
    Class: Payload \n
    Defines a rectangular payload with an aspect ratio as defined by ratios.

    Inputs:
    - mass (defaults to 10) \n
    - density (defaults to 300) \n
    - ratios (defaults to [2, 1, 5]) \n
    """

    mass = Input(10., validator=Positive(incl_zero=True))  # [kg]
    density = Input(300., validator=Positive())  # [kg/m3]
    ratios = Input([2, 1, 5])  # [width, height, length]

    @Attribute
    def volume(self):
        return self.mass / self.density * 1e9  # [mm3]

    @Attribute
    def width(self):
        return (self.volume * self.ratios[0] ** 2 / self.ratios[1] / self.ratios[2]) ** (1/3)

    @Attribute
    def height(self):
        return (self.volume * self.ratios[1] ** 2 / self.ratios[0] / self.ratios[2]) ** (1/3)

    @Attribute
    def length(self):
        return (self.volume * self.ratios[2] ** 2 / self.ratios[0] / self.ratios[1]) ** (1/3)

    @Part
    def payload(self):
        return Box(width=self.length, length=self.width, height=self.height,
                   position=self.position)


if __name__ == '__main__':
    from parapy.gui import display
    obj = Payload()
    display(obj)
