#!/usr/bin/env python3
"""
Hydrone KBE Application \n
Avy B.V.

Propulsion System Classes: \n
- HydrogenTank \n
- FuelCell \n
- Battery \n
- Propeller
"""

__author__ = "Liam Megill"
__date__ = "Tue 01 Mar 2022"
__copyright__ = "Copyright 2022, Avy B.V."

from parapy.core import Input, Part, Attribute
from parapy.geom import GeomBase, Cylinder, Sphere, Box, translate, rotate
from parapy.core.validate import Positive
from parapy.core.widgets import Dropdown
from math import pi
import pandas as pd
from hydrone_app import COMP_DIR
import os


class HydrogenTank(GeomBase):
    """
    HyDrone KBE Application | Avy B.V. \n
    Class: Hydrogen Tank \n
    Defines a simple cylindrical tank with circular caps. Future changes could allow different
    cap shapes, but the current design is sufficient for this analysis. Data is stored in a
    csv file.
    """

    # Read csv file
    tank_data = pd.read_csv(os.path.join(COMP_DIR, 'hydrogen_tanks.csv'), index_col="Name")
    tank_name = Input("MC2L", widget=Dropdown(tank_data.index.values.tolist()))

    # attributes
    @Attribute
    def volume(self):
        return self.tank_data.loc[self.tank_name, "Volume"]

    @Attribute
    def radius(self):
        return self.tank_data.loc[self.tank_name, "Diameter"] / 2

    @Attribute
    def cylindrical_length(self):
        return float(self.tank_data.loc[self.tank_name, "Length"] - self.tank_data.loc[self.tank_name, "Diameter"])

    @Attribute
    def mass(self):
        return self.tank_data.loc[self.tank_name, "Mass"]

    @Attribute
    def pressure(self):
        return self.tank_data.loc[self.tank_name, "Pressure"]

    @Attribute
    def h2_mass(self):
        return self.tank_data.loc[self.tank_name, "Mass_H2"] * 1e-3  # conversion to kg

    @Attribute
    def energy(self):
        return self.tank_data.loc[self.tank_name, "Energy"]

    @Attribute
    def external_volume(self):
        """Volume of full cylinder, ignoring wall thickness. Only for reference, not for calculations."""
        return (self.cylindrical_section.volume + self.fwd_cap.volume + self.aft_cap.volume) * 1e-6

    @Attribute
    def length(self):
        """Full length, including hemispheres. For placement of components."""
        return float(self.tank_data.loc[self.tank_name, "Length"])

    # parts
    @Part
    def cylindrical_section(self):
        return Cylinder(radius=self.radius,
                        height=self.cylindrical_length,
                        position=translate(rotate(translate(self.position, 'x', self.radius), 'y', 90, deg=True),
                                           'x', -self.radius, 'y', self.radius))

    @Part
    def fwd_cap(self):
        return Sphere(radius=self.radius,
                      angle=pi,
                      position=translate(rotate(translate(self.position, 'x', self.radius), 'z', 90, deg=True),
                                         'x', self.radius, 'z', self.radius))

    @Part
    def aft_cap(self):
        return Sphere(radius=self.radius,
                      angle=pi,
                      position=translate(rotate(translate(self.position, 'x', self.radius + self.cylindrical_length),
                                                'z', -90, deg=True),
                                         'x', -self.radius, 'z', self.radius))


class FuelCell(GeomBase):
    """
    HyDrone KBE Application | Avy B.V. \n
    Class: Fuel Cell \n
    Defines a simple rectangular fuel cell. Data is stored in csv file.
    """

    # Read csv file
    fc_data = pd.read_csv(os.path.join(COMP_DIR, 'fuel_cells.csv'), index_col="Name")
    fc_name = Input("IE800", widget=Dropdown(fc_data.index.values.tolist()))

    @Attribute
    def mass(self):
        return float(self.fc_data.loc[self.fc_name, "Mass"])

    @Attribute
    def width(self):
        return float(self.fc_data.loc[self.fc_name, "Width"])

    @Attribute
    def depth(self):
        return float(self.fc_data.loc[self.fc_name, "Depth"])

    @Attribute
    def height(self):
        return float(self.fc_data.loc[self.fc_name, "Height"])

    @Attribute
    def max_cont_power(self):
        return float(self.fc_data.loc[self.fc_name, "Cont_power"])

    @Attribute
    def peak_power(self):
        return float(self.fc_data.loc[self.fc_name, "Peak_power"])

    # parts
    @Part
    def fuel_cell(self):
        return Box(width=self.depth, length=self.width, height=self.height,
                   position=self.position)


class Battery(GeomBase):
    """
    HyDrone KBE Application | Avy B.V. \n
    Class: Battery \n
    Defines a simple rectangular battery. Data is stored in csv file.
    """

    bat_data = pd.read_csv(os.path.join(COMP_DIR, 'foxtech_batteries.csv'), index_col="Name")
    bat_name = Input("12S 27000", widget=Dropdown(bat_data.index.values.tolist()))

    @Attribute
    def height(self):
        return float(self.bat_data.loc[self.bat_name, "Height"])

    @Attribute
    def width(self):
        return float(self.bat_data.loc[self.bat_name, "Width"])

    @Attribute
    def length(self):
        return float(self.bat_data.loc[self.bat_name, "Length"])

    @Attribute
    def mass(self):
        return float(self.bat_data.loc[self.bat_name, "Mass"])

    @Attribute
    def energy(self):
        return float(self.bat_data.loc[self.bat_name, "Energy_Wh"]) * 3600.

    @Part
    def battery(self):
        return Box(width=self.length, length=self.width, height=self.height,
                   position=self.position)


# class ElectricMotor(GeomBase):
#     """"""
#
#     height = Input(validator=Positive())
#     radius = Input(validator=Positive())
#
#     @Part
#     def electric_motor(self):
#         return Cylinder(self.radius, self.height,
#                         position=self.position)
#
#
class Propeller(GeomBase):
    """
    HyDrone KBE Application | Avy B.V. \n
    Class: Battery \n
    Defines a propeller. Currently, a simple cylinder, but this can be improved.
    """

    height = Input(validator=Positive())
    radius = Input(validator=Positive())

    @Part
    def propeller(self):
        return Cylinder(radius=self.radius, height=self.height,
                        position=self.position)


if __name__ == '__main__':
    from parapy.gui import display
    obj = HydrogenTank()
    display(obj)
